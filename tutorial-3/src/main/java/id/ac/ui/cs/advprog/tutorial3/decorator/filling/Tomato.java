package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        this.food = food;
        this.description = "Tomato";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + ", adding " + this.description.toLowerCase();
    }

    @Override
    public double cost() {
        return 0.5 + food.cost();
    }
}
