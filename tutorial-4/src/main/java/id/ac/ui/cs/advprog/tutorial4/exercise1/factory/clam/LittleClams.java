package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class LittleClams implements Clams {

    public String toString() {
        return "Little Clams";
    }
}