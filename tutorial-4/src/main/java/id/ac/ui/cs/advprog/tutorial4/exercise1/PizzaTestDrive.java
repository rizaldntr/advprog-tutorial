package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();
        PizzaStore depokStore = new DepokPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        Pizza pizzaDepok = depokStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizzaDepok + "\n");

        pizzaDepok = depokStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizzaDepok + "\n");

        pizzaDepok = depokStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizzaDepok + "\n");
    }
}
