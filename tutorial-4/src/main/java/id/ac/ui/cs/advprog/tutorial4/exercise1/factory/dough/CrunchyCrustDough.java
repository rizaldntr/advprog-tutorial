package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CrunchyCrustDough implements Dough {
    public String toString() {
        return "CrunchyCrust style extra crunchy crust dough";
    }
}
